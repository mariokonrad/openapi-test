#ifndef HTTP__HPP
#define HTTP__HPP

#include <iosfwd>
#include <string>
#include <map>

struct http {
	enum class method { none, get, put, post, del, head, patch };

	// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
	enum class code {
		// 1xx : informational
		do_continue = 100,
		switching_protocols = 101,
		processing = 102,

		// 2xx : success
		ok = 200,
		created = 201,
		accepted = 202,
		non_authoritative_information = 203,
		no_content = 204,
		reset_content = 205,
		partial_content = 206,
		multi_status = 207,
		already_reported = 208,
		im_used = 209,

		// 3xx : redirection
		multiple_choices = 300,
		moved_permanently = 301,
		found = 302,
		see_other = 303,
		not_modified = 304,
		use_proxy = 305,
		temporary_redirect = 307,
		permanent_redirect = 308,

		// 4xx : client error
		bad_request = 400,
		unauthorized = 401,
		payment_required = 402,
		forbidden = 403,
		not_found = 404,
		method_not_allowed = 405,
		not_acceptable = 406,
		proxy_authentication_required = 407,
		request_timeout = 408,
		conflict = 409,
		gone = 410,
		length_required = 411,
		precondition_failed = 412,
		payload_too_large = 413,
		request_uri_too_long = 414,
		unsupported_media_type = 415,
		requested_range_not_satisfiable = 416,
		expectation_failed = 417,
		i_am_a_teapot = 418,
		misdirected_request = 421,
		unprocessable_entity = 422,
		locked = 423,
		failed_dependency = 424,
		upgrade_required = 426,
		precondition_required = 428,
		too_many_requests = 429,
		request_header_fields_too_large = 431,
		connection_closed_without_response = 444,
		unavailable_for_legal_reasons = 451,
		client_closed_request = 499,

		// 5xx : server error
		internal_server_error = 500,
		not_implemented = 501,
		bad_gateway = 502,
		service_unavailable = 503,
		gateway_timeout = 504,
		http_version_not_supported = 505,
		variant_also_negotiates = 506,
		insufficient_storage = 507,
		loop_detected = 508,
		not_extended = 510,
		network_authentication_required = 511,
		network_connect_timeout_error = 599,
	};

	struct request {
		http::method method;
		std::string path;
		std::string protocol;
		std::map<std::string, std::string> header;
		std::map<std::string, std::string> query;
		std::string body;
	};

	struct response {
	};
};

class http_exception
{
public:
	http_exception(http::code code)
		: code_(code)
	{
	}

	const char * what() const noexcept { return "http exception"; }

	http::code code() const noexcept { return code_; }

private:
	http::code code_;
};

std::ostream & operator<<(std::ostream & os, http::code code);
std::ostream & operator<<(std::ostream & os, http::method m);
http::method to_method(const std::string & s);
http::method to_method_case(std::string s);
http::request parse(const std::string & s);

#endif
