#include <string>
#include <map>
#include <vector>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <memory>

#include <yaml-cpp/yaml.h>
#include <json-schema.hpp>

#include "http.hpp"
#include "split.hpp"
#include "schema.hpp"

struct method_descriptor {
	http::method method;
	std::string operation_id;
};

struct handler_entry {
	std::string path;
	std::vector<method_descriptor> mh;
};

std::vector<handler_entry>::const_iterator find_path(
	const std::vector<handler_entry> & c, const http::request & req)
{
	const std::vector<std::string> path_parts = split(req.path, "/");
	if (path_parts.empty())
		throw std::runtime_error{"invalid path in request"};

	for (auto i = std::begin(c); i != std::end(c); ++i) {
		const std::vector<std::string> parts = split(i->path, "/");
		if (path_parts.size() != parts.size())
			continue;

		std::size_t j = 0u;
		for (; j < path_parts.size(); ++j) {
			if (parts[j].size() <= 2u)
				throw std::runtime_error{"invalid path variable format"};
			const std::string & p = path_parts[j];
			const std::string & q = parts[j];

			if ((q[0] == '{') && (q[q.size() - 1u] == '}'))
				continue;

			if (p != q)
				break;
		}

		if (j == path_parts.size())
			return i;
	}
	return std::end(c);
}

struct {
	std::map<std::string, std::function<void(const http::request &, http::response &)>>
		handler_map;

	std::vector<handler_entry> handlers;

	std::map<std::string, std::shared_ptr<schema::validator::node>> schemas;

	std::map<std::string,
		std::function<std::shared_ptr<schema::validator::node>(const YAML::Node &)>>
		factory_validator;

} global;

void process(const std::string & text, http::response & res)
{
	const auto req = parse(text);

	const auto path_handler = find_path(global.handlers, req);
	if (path_handler == std::cend(global.handlers))
		throw http_exception{http::code::not_found};

	const auto path_method_handler
		= std::find_if(std::cbegin(path_handler->mh), std::cend(path_handler->mh),
			[&](const method_descriptor & d) { return req.method == d.method; });
	if (path_method_handler == std::cend(path_handler->mh))
		throw http_exception{http::code::method_not_allowed};

	const auto proc = global.handler_map.find(path_method_handler->operation_id);
	if (proc == std::cend(global.handler_map))
		throw http_exception{http::code::internal_server_error};

	proc->second(req, res);
}

void register_operation(const std::string & operation_id,
	std::function<void(const http::request &, http::response &)> handler)
{
	global.handler_map.try_emplace(operation_id, handler);
}

void register_paths(const YAML::Node & paths)
{
	if (!paths.IsMap())
		throw std::runtime_error{"schema error: expected paths underneath 'paths'"};
	for (const auto & path : paths) {
		if (!path.second.IsMap())
			throw std::runtime_error{"schema error: expected methods underneath path"};

		handler_entry entry;
		entry.path = path.first.as<std::string>();

		for (const auto & method : path.second) {
			if (method.second.IsSequence()) // case: parameters, etc.
				continue;

			if (!method.second.IsMap())
				throw std::runtime_error{
					"schema error: expected object underneath path/method"};

			const auto m = to_method_case(method.first.as<std::string>());
			if (m == http::method::none)
				continue;

			const auto & operation_id = method.second["operationId"];
			if (!operation_id.IsScalar())
				throw std::runtime_error{
					"schema error: expected scalar type for 'operationId'"};

			entry.mh.push_back({m, operation_id.as<std::string>()});
		}

		global.handlers.push_back(entry);
	}
}

std::shared_ptr<schema::validator::node> make_validator(const YAML::Node & schema)
{
	const YAML::Node & t = schema["type"];
	if (!t.IsDefined())
		throw std::domain_error{"schema error: field 'type' not defined in schema"};

	const std::string type = t.as<std::string>();

	const auto factory = global.factory_validator.find(type);
	if (factory == std::end(global.factory_validator))
		throw std::domain_error{"schema error: unsupported type"};

	return factory->second(schema);
}

void register_schemas(const YAML::Node & schemas)
{
	if (!schemas.IsDefined())
		throw std::runtime_error{"schema error: no schemas defined"};
	if (!schemas.IsMap())
		throw std::runtime_error{"schema error: expected object at 'components/schemas'"};

	for (const auto & schema : schemas) {
		if (!schema.second.IsMap())
			continue;

		global.schemas.emplace(schema.first.as<std::string>(), make_validator(schema.second));
	}
}

void register_api(const std::string & spec_filename)
{
	const YAML::Node node = YAML::LoadFile(spec_filename);

	if (const YAML::Node & components = node["components"];
		components.IsDefined() && components.IsMap()) {
		register_schemas(components["schemas"]);
	}
	register_paths(node["paths"]);
}

template <class T, class V> void read_pattern(const YAML::Node & schema, std::shared_ptr<V> & v)
{
	if (const auto a = schema["pattern"]; a.IsDefined()) {
		if (!a.IsScalar())
			throw std::domain_error{"schema error: field 'pattern' must be a string"};
		v->set_pattern(a.as<T>());
	}
}

template <class T, class V> void read_default(const YAML::Node & schema, std::shared_ptr<V> & v)
{
	if (const auto a = schema["default"]; a.IsDefined()) {
		if (!a.IsScalar())
			throw std::domain_error{"schema error: field 'default' must be a scalar type"};
		v->set_default(a.as<T>());
	}
}

std::shared_ptr<schema::validator::node> make_validator_string(const YAML::Node & schema)
{
	auto v = std::make_shared<schema::validator::string>();

	read_pattern<std::string>(schema, v);
	read_default<std::string>(schema, v);
	// TODO

	return v;
}

std::shared_ptr<schema::validator::node> make_validator_boolean(const YAML::Node & schema)
{
	auto v = std::make_shared<schema::validator::boolean>();

	read_default<bool>(schema, v);
	// TODO

	return v;
}

std::shared_ptr<schema::validator::node> make_validator_integer(const YAML::Node & /*schema*/)
{
	auto v = std::make_shared<schema::validator::integer>();
	// TODO
	return v;
}

std::shared_ptr<schema::validator::node> make_validator_number(const YAML::Node & /*schema*/)
{
	auto v = std::make_shared<schema::validator::number>();
	// TODO
	return v;
}

std::shared_ptr<schema::validator::node> make_validator_array(const YAML::Node & /*schema*/)
{
	auto v = std::make_shared<schema::validator::array>();
	// TODO
	return v;
}

std::shared_ptr<schema::validator::node> make_validator_object(const YAML::Node & /*schema*/)
{
	auto v = std::make_shared<schema::validator::object>();
	// TODO
	return v;
}

void get_info_foo_bar(const http::request &, http::response &)
{
	std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << "\n";
}

void put_info_foo_bar(const http::request &, http::response &)
{
	std::cout << __PRETTY_FUNCTION__ << ":" << __LINE__ << "\n";
}

/* copied as an example from `json-schema-validator/README.md`
#include "json-schema.hpp"

using nlohmann::json;
using nlohmann::json_uri;
using nlohmann::json_schema_draft4::json_validator;

static void loader(const json_uri &uri, json &schema)
{
    // get the schema from uri and feed it into schema
    // if not possible, otherwise, throw an excpetion
}

int main(void)
{
    json schema;

    // json-parse the schema

    json_validator validator(loader); // create validator with a loader-callback

    try {
        validator.set_root_schema(schema); // insert root-schema
    } catch (const std::exception &e) {
        std::cerr << "Validation failed, here is why: " << e.what() << "\n";
        return EXIT_FAILURE;
    }

    json document;

    // json-parse the document

    try {
        validator.validate(document); // validate the document
    } catch (const std::exception &e) {
        std::cerr << "Validation failed, here is why: " << e.what() << "\n";
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
*/

int main(int, char **)
{
	global.factory_validator.emplace("string", make_validator_string);
	global.factory_validator.emplace("boolean", make_validator_boolean);
	global.factory_validator.emplace("integer", make_validator_integer);
	global.factory_validator.emplace("number", make_validator_number);
	global.factory_validator.emplace("array", make_validator_array);
	global.factory_validator.emplace("object", make_validator_object);

	register_operation("get_info_foo_bar", get_info_foo_bar);
	register_operation("put_info_foo_bar", put_info_foo_bar);

	register_api("api.yaml");

	for (const auto & schema : global.schemas) {
		std::cout << "schema: " << schema.first << "\n";
	}
	for (const auto & handler : global.handlers) {
		for (const auto & method : handler.mh) {
			std::cout << "path: " << method.method << " @ " << handler.path << "\n";
		}
	}

	static const char * text = "GET /info/foo/bar HTTP/1.1\r\n"
							   "Content: text/plain\r\n"
							   "\r\n";

	try {
		http::response res;
		process(text, res);
	} catch (http_exception & e) {
		std::cout << e.what() << " / " << e.code() << '\n';
	} catch (std::exception & e) {
		std::cout << e.what() << '\n';
	}
}
