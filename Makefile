ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
INSTALL_PREFIX:=$(ROOT_DIR)/local

CXX=g++-7
CXXFLAGS=-std=c++17 -ggdb -Og -I$(INSTALL_PREFIX)/include -Iextern/json-schema -Iextern
LDFLAGS=-L$(INSTALL_PREFIX)/lib
AR=ar
ARFLAGS=rcs

OBJECTS=main.o http.o split.o

main : $(OBJECTS) libjson-schema.a
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS) -lyaml-cpp -L. -ljson-schema

libjson-schema.a : json-schema-draft4.json.o json-uri.o json-validator.o
	$(AR) $(ARFLAGS) $@ $^

.PHONY: yaml
yaml :
	rm -fr build \
	&& mkdir build \
	&& cd build \
	&& pwd \
	&& cmake $(HOME)/local/repo/yaml-cpp \
		-DCMAKE_INSTALL_PREFIX=$(INSTALL_PREFIX) \
		-DYAML_CPP_BUILD_TESTS=NO \
		-DYAML_CPP_BUILD_TOOLS=NO \
		-DYAML_CPP_BUILD_CONTRIB=NO \
		-DBUILD_SHARED_LIBS=NO \
	&& $(MAKE) yaml-cpp install \
	&& cd - \
	&& rm -fr build

.PHONY: clean
clean :
	rm -f main
	rm -f *.o
	rm -f *.a
	rm -fr $(INSTALL_PREFIX)

%.o : src/%.cpp
	$(CXX) -Wall -Wextra -Werror -pedantic $(CXXFLAGS) -c $< -o $@

%.o : extern/json-schema/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
