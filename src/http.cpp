#include "http.hpp"
#include <ostream>
#include <stdexcept>
#include <vector>
#include "split.hpp"

namespace detail
{
static constexpr const char * to_cstring(http::method m) noexcept
{
	switch (m) {
		case http::method::get:
			return "GET";
		case http::method::put:
			return "PUT";
		case http::method::post:
			return "POST";
		case http::method::del:
			return "DELETE";
		case http::method::head:
			return "HEAD";
		case http::method::patch:
			return "PATCH";
		case http::method::none:
			return "<none>";
	}
	return "<unreachable>"; // shuts the compiler up
}
}

std::ostream & operator<<(std::ostream & os, http::code code)
{
	return os << static_cast<int>(code);
}

std::ostream & operator<<(std::ostream & os, http::method m)
{
	return os << detail::to_cstring(m);
}

http::method to_method(const std::string & s)
{
	if (s == "GET")
		return http::method::get;
	if (s == "PUT")
		return http::method::put;
	if (s == "POST")
		return http::method::post;
	if (s == "DELETE")
		return http::method::del;
	if (s == "HEAD")
		return http::method::head;
	if (s == "PATCH")
		return http::method::patch;
	return http::method::none;
}

http::method to_method_case(std::string s)
{
	for (auto & c : s)
		c = toupper(c);
	return to_method(s);
}

http::request parse(const std::string & s)
{
	http::request req;

	std::vector<std::string> lines
		= split(s, "\r\n");
	if (lines.size() == 0u)
		throw http_exception{http::code::bad_request};

	std::vector<std::string> t = split(lines[0], " ");
	if (t.size() != 3u)
		throw http_exception{http::code::bad_request};

	req.method = to_method(t[0]);
	req.path = t[1];
	req.protocol = t[2];
	lines.erase(lines.begin());

	for (const auto & l : lines) {
		std::vector<std::string> h = split(l, " :");
		if (h.size() != 2u)
			throw http_exception{http::code::bad_request};
		req.header.insert({h[0], h[1]});
	}

	return req;
}

