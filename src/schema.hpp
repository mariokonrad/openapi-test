#ifndef SCHEMA__HPP
#define SCHEMA__HPP

#include <string>
#include <vector>
#include <memory>

namespace schema::validator
{
class exception
{
};

struct data;

class node
{
public:
	virtual void validate(const data &) const = 0;
};

class boolean : public node
{
public:
	virtual void validate(const data &) const override {}

	void set_default(bool value) { default_ = value; }
	bool get_default() const { return default_; }

private:
	bool default_ = false;
};

class integer : public node
{
public:
	virtual void validate(const data &) const override {}
};

class number : public node
{
public:
	virtual void validate(const data &) const override {}
};

class string : public node
{
public:
	virtual void validate(const data &) const override {}

	void set_pattern(const std::string & p) { pattern_ = p; }
	const std::string get_pattern() const { return pattern_; }

	void set_default(const std::string & value) { default_ = value; }
	const std::string & get_default() const { return default_; }

private:
	std::string pattern_;
	std::string default_;
};

class object : public node
{
public:
	virtual void validate(const data &) const override {}

private:
	std::vector<std::unique_ptr<node>> nodes_;
};

class array : public node
{
public:
	virtual void validate(const data &) const override {}

private:
	std::vector<std::unique_ptr<node>> nodes_;
};
}

#endif
