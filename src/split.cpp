#include "split.hpp"

std::vector<std::string> split(const std::string & s, const std::string & delm)
{
	std::vector<std::string> c;
	std::string::size_type last = s.find_first_not_of(delm, 0);
	std::string::size_type p = s.find_first_of(delm, last);
	for (; p != std::string::npos || last != std::string::npos;) {
		c.push_back(s.substr(last, p - last));
		last = s.find_first_not_of(delm, p);
		p = s.find_first_of(delm, last);
	}
	return c;
}
