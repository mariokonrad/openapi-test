#ifndef SPLIT__HPP
#define SPLIT__HPP

#include <string>
#include <vector>

std::vector<std::string> split(const std::string & s, const std::string & delm);

#endif
